package cs3500.hw02;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ValuesTest {


  Values ace;
  Values two;
  Values three;
  Values four;
  Values five;
  Values six;
  Values seven;
  Values eight;
  Values nine;
  Values ten;
  Values jack;
  Values queen;
  Values king;

  /**
   * Initailizes all of the variables.
   */
  @Before
  public void initialize() {
    this.ace = Values.Ace;
    this.two = Values.Two;
    this.three = Values.Three;
    this.four = Values.Four;
    this.five = Values.Five;
    this.six = Values.Six;
    this.seven = Values.Seven;
    this.eight = Values.Eight;
    this.nine = Values.Nine;
    this.ten = Values.Ten;
    this.jack = Values.Jack;
    this.queen = Values.Queen;
    this.king = Values.King;

  }


  @Test
  public void testToStringAce() {
    assertEquals("A", this.ace.toString());
  }

  @Test
  public void testToStringEight() {
    assertEquals("8", this.eight.toString());
  }

  @Test
  public void getValueNine() {
    assertEquals(9, this.nine.getValue());
  }

  @Test
  public void getValueTen() {
    assertEquals(10, this.ten.getValue());
  }

}