package cs3500.hw02;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SuitsTest {


  Suits heart = Suits.Heart;
  Suits spade = Suits.Spade;
  Suits diamond = Suits.Diamond;
  Suits club = Suits.Club;


  @Test
  public void oppositeColorsheartSpade() {
    assertEquals(true, heart.oppositeColors(spade));
  }

  @Test
  public void oppositeColorsClubDiamond() {
    assertEquals(true, club.oppositeColors(diamond));
  }

  @Test
  public void oppositeColorsheartheart() {
    assertEquals(false, heart.oppositeColors(heart));
  }

  @Test
  public void oppositeColorsheartDiamond() {
    assertEquals(false, heart.oppositeColors(diamond));
  }

  @Test
  public void hearttoString() {
    assertEquals(Character.toString('\u2665'), heart.toString());
  }

}