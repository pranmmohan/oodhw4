
package cs3500.hw02;

import static org.junit.Assert.assertEquals;

import cs3500.hw04.FreecellModelCreator;
import cs3500.hw04.FreecellModelCreator.GameType;
import java.util.List;
import java.util.Stack;

import org.junit.Test;

/**
 * Tests for the single move and multi move model.
 */
public abstract class FactoryFreecellModelTest {


  protected abstract FreecellOperations<Card> freeCellFactory();

  /**
   * Runs all tests in this class as both Single move and Multi move models.
   */
  public static final class Single extends FactoryFreecellModelTest {

    @Override
    protected FreecellOperations<Card> freeCellFactory() {
      return FreecellModelCreator.create(GameType.SINGLEMOVE);
    }
  }

  public static final class Multi extends FactoryFreecellModelTest {

    @Override
    protected FreecellOperations<Card> freeCellFactory() {
      return FreecellModelCreator.create(GameType.MULTIMOVE);
    }

  }

  FreecellOperations<Card> emptyfreeCellFactory = freeCellFactory();
  FreecellOperations<Card> toCascade = freeCellFactory();
  FreecellOperations<Card> toFoundation = freeCellFactory();
  FreecellOperations<Card> toOpenShuffle = freeCellFactory();
  FreecellOperations<Card> noShuffle = freeCellFactory();
  FreecellOperations<Card> emptyOpen = freeCellFactory();
  FreecellOperations<Card> shuffled = freeCellFactory();


  /**
   * Checks whether shuffling a deck produces a deck with 52
   * cards in random positions.
   */
  @Test
  public void shuffleDeck() {
    emptyfreeCellFactory.startGame(emptyfreeCellFactory.getDeck(),
        8, 4, true);
    assertEquals(52, this.emptyfreeCellFactory.getDeck().size());
  }




  @Test(expected = IllegalArgumentException.class)
  public void invalidmoveToCascade() {
    toCascade.startGame(toCascade.getDeck(), 8, 5,
        true);

    toCascade.move(PileType.CASCADE, 5, 8, PileType.FOUNDATION, 0);

  }

  @Test
  public void moveToCascade() {
    toCascade = freeCellFactory();
    toCascade.startGame(toCascade.getDeck(), 8, 5,
        false);

    toCascade.move(PileType.CASCADE, 0, 6, PileType.OPEN, 0);
    toCascade.move(PileType.OPEN, 0, 0, PileType.CASCADE, 7 );
    assertEquals("F1:\n"
        + "F2:\n"
        + "F3:\n"
        + "F4:\n"
        + "O1:\n"
        + "O2:\n"
        + "O3:\n"
        + "O4:\n"
        + "O5:\n"
        + "C1: K♥, J♥, 9♥, 7♥, 5♥, 3♥\n"
        + "C2: K♣, J♣, 9♣, 7♣, 5♣, 3♣, A♣\n"
        + "C3: K♦, J♦, 9♦, 7♦, 5♦, 3♦, A♦\n"
        + "C4: K♠, J♠, 9♠, 7♠, 5♠, 3♠, A♠\n"
        + "C5: Q♥, 10♥, 8♥, 6♥, 4♥, 2♥\n"
        + "C6: Q♣, 10♣, 8♣, 6♣, 4♣, 2♣\n"
        + "C7: Q♦, 10♦, 8♦, 6♦, 4♦, 2♦\n"
        + "C8: Q♠, 10♠, 8♠, 6♠, 4♠, 2♠, A♥", toCascade.getGameState());



  }

  @Test(expected = IllegalArgumentException.class)
  public void invalidmoveToFoundation() {
    toCascade.startGame(toCascade.getDeck(), 8, 5,
        true);

    toCascade.move(PileType.CASCADE, 5, 8, PileType.CASCADE, 0);


  }


  /**
   * Valid move from a cascade pile to a non empty foundation and empty foundation.
   */
  @Test
  public void moveToFoundation() {
    toFoundation.startGame(toFoundation.getDeck(), 8,
        3, false);

    toFoundation.move(PileType.CASCADE, 0, 6, PileType.OPEN, 0);

    String output = "F1:\n"
        + "F2:\n"
        + "F3:\n"
        + "F4:\n"
        + "O1: A♥\n"
        + "O2:\n"
        + "O3:\n"
        + "C1: K♥, J♥, 9♥, 7♥, 5♥, 3♥\n"
        + "C2: K♣, J♣, 9♣, 7♣, 5♣, 3♣, A♣\n"
        + "C3: K♦, J♦, 9♦, 7♦, 5♦, 3♦, A♦\n"
        + "C4: K♠, J♠, 9♠, 7♠, 5♠, 3♠, A♠\n"
        + "C5: Q♥, 10♥, 8♥, 6♥, 4♥, 2♥\n"
        + "C6: Q♣, 10♣, 8♣, 6♣, 4♣, 2♣\n"
        + "C7: Q♦, 10♦, 8♦, 6♦, 4♦, 2♦\n"
        + "C8: Q♠, 10♠, 8♠, 6♠, 4♠, 2♠";
    assertEquals(output, toFoundation.getGameState());

  }




  /**
   * Valid move from an open pile to another empty
   * open pile with a nonshuffled deck.
   */
  @Test
  public void moveToEmptyOpenNoShuffle() {
    emptyfreeCellFactory.startGame(emptyfreeCellFactory.getDeck(),
        8, 4, false);
    emptyfreeCellFactory.move(PileType.CASCADE, 0, 6, PileType.OPEN,
        1);
    assertEquals("F1:\n"
        + "F2:\n"
        + "F3:\n"
        + "F4:\n"
        + "O1:\n"
        + "O2: A♥\n"
        + "O3:\n"
        + "O4:\n"
        + "C1: K♥, J♥, 9♥, 7♥, 5♥, 3♥\n"
        + "C2: K♣, J♣, 9♣, 7♣, 5♣, 3♣, A♣\n"
        + "C3: K♦, J♦, 9♦, 7♦, 5♦, 3♦, A♦\n"
        + "C4: K♠, J♠, 9♠, 7♠, 5♠, 3♠, A♠\n"
        + "C5: Q♥, 10♥, 8♥, 6♥, 4♥, 2♥\n"
        + "C6: Q♣, 10♣, 8♣, 6♣, 4♣, 2♣\n"
        + "C7: Q♦, 10♦, 8♦, 6♦, 4♦, 2♦\n"
        + "C8: Q♠, 10♠, 8♠, 6♠, 4♠, 2♠", emptyfreeCellFactory.getGameState());


  }


  /**
   * Valid move from an open pile to another empty
   * open pile with a valid shuffled deck.
   */
  @Test
  public void moveToOpenShuffle() {
    toOpenShuffle.startGame(toOpenShuffle.getDeck(),
        8, 4, true);
    toOpenShuffle.move(PileType.CASCADE, 0, 6, PileType.OPEN,
        1);
    String outputSize = "F1:\n"
        + "F2:\n"
        + "F3:\n"
        + "F4:\n"
        + "O1:\n"
        + "O2: A♥\n"
        + "O3:\n"
        + "O4:\n"
        + "C1: K♥, J♥, 9♥, 7♥, 5♥, 3♥\n"
        + "C2: K♣, J♣, 9♣, 7♣, 5♣, 3♣, A♣\n"
        + "C3: K♦, J♦, 9♦, 7♦, 5♦, 3♦, A♦\n"
        + "C4: K♠, J♠, 9♠, 7♠, 5♠, 3♠, A♠\n"
        + "C5: Q♥, 10♥, 8♥, 6♥, 4♥, 2♥\n"
        + "C6: Q♣, 10♣, 8♣, 6♣, 4♣, 2♣\n"
        + "C7: Q♦, 10♦, 8♦, 6♦, 4♦, 2♦\n"
        + "C8: Q♠, 10♠, 8♠, 6♠, 4♠, 2♠";

    assertEquals(outputSize.length(), toOpenShuffle.getGameState().length());



  }

  /**
   * Valid move with no shuffle.
   */
  @Test(expected = IllegalArgumentException.class)
  public void moveToNonEmptyShuffle() {
    toOpenShuffle.startGame(toOpenShuffle.getDeck(),
        8, 4, true);
    toOpenShuffle.move(PileType.CASCADE, 0, 7, PileType.OPEN,
        1);
    toOpenShuffle.move(PileType.CASCADE, 0, 6, PileType.OPEN,
        1);
  }

  /**
   * Checks if there are any duplicate cards
   * without shuffling the deck.
   */
  @Test
  public void noDuplicateCardsNoShuffle() {
    noShuffle.startGame(emptyfreeCellFactory.getDeck(),
        8, 4, false);

    int numHeartJack = 0;

    for (int i = 0; i < noShuffle.getDeck().size(); i++) {
      if (noShuffle.getDeck().get(i).getValue() == Values.Ace
          && noShuffle.getDeck().get(i).getSuit() == Suits.Heart) {
        numHeartJack++;
      }
    }

    assertEquals(1, numHeartJack);
  }


  /**
   * Checks if there are duplicates in the deck after shuffling.
   */
  @Test
  public void noDuplicationShuffle() {
    noShuffle.startGame(emptyfreeCellFactory.getDeck(),
        8, 4, true);

    int numHeartJack = 0;

    for (int i = 0; i < noShuffle.getDeck().size(); i++) {
      if (noShuffle.getDeck().get(i).getValue() == Values.Ace
          && noShuffle.getDeck().get(i).getSuit() == Suits.Heart) {
        numHeartJack++;
      }
    }

    assertEquals(1, numHeartJack);

  }


  /**
   * Try to move from an Empty Pile.
   */
  @Test(expected = IllegalArgumentException.class)
  public void EmptyOpenPile() {
    emptyOpen.startGame(emptyOpen.getDeck(), 5,
        2, false);


    emptyOpen.move(PileType.OPEN, 0, 0,
        PileType.OPEN, 1);

  }

  /**
   * Checks if the non shuffled deck is complete.
   */
  @Test(expected = IllegalArgumentException.class)
  public void IncompleteDeckNoShuffle() {
    List<Card> deck = new Stack<Card>();
    noShuffle.startGame(deck, 5,
        2, false);

  }


  /**
   * Checks if the shuffled deck is complete.
   */
  @Test(expected = IllegalArgumentException.class)
  public void IncompleteDeckShuffle() {
    List<Card> deck = new Stack<Card>();
    shuffled.startGame(deck, 5,
        2, false);

  }




  /**
   * Cards are dealt properly without shuffling.
   */

  @Test
  public void goodDealNoShuffle() {
    noShuffle = freeCellFactory();
    noShuffle.startGame(noShuffle.getDeck(), 6, 4, false);
    String gameState = "F1:\n"
        + "F2:\n"
        + "F3:\n"
        + "F4:\n"
        + "O1:\n"
        + "O2:\n"
        + "O3:\n"
        + "O4:\n"
        + "C1: K♥, Q♦, 10♥, 9♦, 7♥, 6♦, 4♥, 3♦, A♥\n"
        + "C2: K♣, Q♠, 10♣, 9♠, 7♣, 6♠, 4♣, 3♠, A♣\n"
        + "C3: K♦, J♥, 10♦, 8♥, 7♦, 5♥, 4♦, 2♥, A♦\n"
        + "C4: K♠, J♣, 10♠, 8♣, 7♠, 5♣, 4♠, 2♣, A♠\n"
        + "C5: Q♥, J♦, 9♥, 8♦, 6♥, 5♦, 3♥, 2♦\n"
        + "C6: Q♣, J♠, 9♣, 8♠, 6♣, 5♠, 3♣, 2♠";
    assertEquals(gameState.length(), noShuffle.getGameState().length());

  }

  /**
   * Cards are dealt properly after shuffling.
   */

  @Test
  public void goodDealShuffle() {

    shuffled.startGame(shuffled.getDeck(), 6, 4, false);

    String gameState = "F1:\n"
        + "F2:\n"
        + "F3:\n"
        + "F4:\n"
        + "O1:\n"
        + "O2:\n"
        + "O3:\n"
        + "O4:\n"
        + "C1: K♥, Q♦, 10♥, 9♦, 7♥, 6♦, 4♥, 3♦, A♥\n"
        + "C2: K♣, Q♠, 10♣, 9♠, 7♣, 6♠, 4♣, 3♠, A♣\n"
        + "C3: K♦, J♥, 10♦, 8♥, 7♦, 5♥, 4♦, 2♥, A♦\n"
        + "C4: K♠, J♣, 10♠, 8♣, 7♠, 5♣, 4♠, 2♣, A♠\n"
        + "C5: Q♥, J♦, 9♥, 8♦, 6♥, 5♦, 3♥, 2♦\n"
        + "C6: Q♣, J♠, 9♣, 8♠, 6♣, 5♠, 3♣, 2♠";

    assertEquals(gameState.length(), shuffled.getGameState().length());
  }


  /**
   * Duplicate cards in the deck after shuffling.
   */

  @Test(expected = IllegalArgumentException.class)
  public void duplicatesInShuffle() {
    shuffled = freeCellFactory();
    List<Card> shuffledDeck = shuffled.getDeck();
    shuffledDeck.add(new Card(Values.Ace, Suits.Spade));

    shuffled.startGame(shuffledDeck, 6,
        8, true );
  }


  /**
   * Duplicate cards in the deck without shuffling.
   */
  @Test(expected = IllegalArgumentException.class)
  public void duplicatesInNoShuffle() {
    noShuffle = freeCellFactory();
    List<Card> noShuffledDeck = noShuffle.getDeck();
    noShuffledDeck.add(new Card(Values.Five, Suits.Diamond));


    noShuffle.startGame(noShuffledDeck, 6,
        8, false);


  }

  /**
   * Invalid number of cascade piles as an argument.
   */
  @Test(expected = IllegalArgumentException.class)
  public void invalidNumCascades() {
    shuffled = freeCellFactory();
    shuffled.startGame(shuffled.getDeck(), 3,
        1, true);
  }

  /**
   * Invalid number of open piles as an argument.
   */
  @Test(expected = IllegalArgumentException.class)
  public void invalidNumOpens() {
    shuffled = freeCellFactory();
    shuffled.startGame(shuffled.getDeck(), 5,
        0, true);

  }


  /**
   * Cards are dealt properly with no shuffle.
   */
  @Test
  public void testDealCorrectlyNoShuffle() {
    String gameState = "F1:\n"
        + "F2:\n"
        + "F3:\n"
        + "F4:\n"
        + "O1:\n"
        + "O2:\n"
        + "O3:\n"
        + "O4:\n"
        + "C1: K♥, Q♦, 10♥, 9♦, 7♥, 6♦, 4♥, 3♦, A♥\n"
        + "C2: K♣, Q♠, 10♣, 9♠, 7♣, 6♠, 4♣, 3♠, A♣\n"
        + "C3: K♦, J♥, 10♦, 8♥, 7♦, 5♥, 4♦, 2♥, A♦\n"
        + "C4: K♠, J♣, 10♠, 8♣, 7♠, 5♣, 4♠, 2♣, A♠\n"
        + "C5: Q♥, J♦, 9♥, 8♦, 6♥, 5♦, 3♥, 2♦\n"
        + "C6: Q♣, J♠, 9♣, 8♠, 6♣, 5♠, 3♣, 2♠";

    noShuffle = freeCellFactory();
    noShuffle.startGame(noShuffle.getDeck(), 6, 4,
        false);
    assertEquals(gameState, noShuffle.getGameState());
  }


  /**
   * Nothing is displayed if the game has not started.
   */
  @Test
  public void gameStateNoStart() {
    noShuffle = freeCellFactory();
    assertEquals("", noShuffle.getGameState());

  }

  /**
   * Piles are displayed properly after a card is moved.
   */
  @Test
  public void gameStateAfterMove() {
    noShuffle = freeCellFactory();
    noShuffle.startGame(noShuffle.getDeck(), 6, 4,
        false);
    noShuffle.move(PileType.CASCADE, 2, 8, PileType.OPEN, 0);
    String gameState = "F1:\n"
        + "F2:\n"
        + "F3:\n"
        + "F4:\n"
        + "O1: A♦\n"
        + "O2:\n"
        + "O3:\n"
        + "O4:\n"
        + "C1: K♥, Q♦, 10♥, 9♦, 7♥, 6♦, 4♥, 3♦, A♥\n"
        + "C2: K♣, Q♠, 10♣, 9♠, 7♣, 6♠, 4♣, 3♠, A♣\n"
        + "C3: K♦, J♥, 10♦, 8♥, 7♦, 5♥, 4♦, 2♥\n"
        + "C4: K♠, J♣, 10♠, 8♣, 7♠, 5♣, 4♠, 2♣, A♠\n"
        + "C5: Q♥, J♦, 9♥, 8♦, 6♥, 5♦, 3♥, 2♦\n"
        + "C6: Q♣, J♠, 9♣, 8♠, 6♣, 5♠, 3♣, 2♠";

    assertEquals(gameState, noShuffle.getGameState());
  }

  /**
   * Tests the game is not over.
   */
  @Test
  public void gameNotOver() {
    noShuffle = freeCellFactory();
    noShuffle.startGame(noShuffle.getDeck(), 6, 4,
        false);
    assertEquals(false, noShuffle.isGameOver());
  }

  /**
   * Tests the game is over.
   */
  @Test
  public void gameOver() {
    noShuffle = freeCellFactory();
    noShuffle.startGame(noShuffle.getDeck(), 4, 4,
        false);
    noShuffle.move(PileType.CASCADE, 0, 12, PileType.FOUNDATION, 0);
    noShuffle.move(PileType.CASCADE, 0, 11, PileType.FOUNDATION, 0);
    noShuffle.move(PileType.CASCADE, 0, 10, PileType.FOUNDATION, 0);
    noShuffle.move(PileType.CASCADE, 0, 9, PileType.FOUNDATION, 0);
    noShuffle.move(PileType.CASCADE, 0, 8, PileType.FOUNDATION, 0);
    noShuffle.move(PileType.CASCADE, 0, 7, PileType.FOUNDATION, 0);
    noShuffle.move(PileType.CASCADE, 0, 6, PileType.FOUNDATION, 0);
    noShuffle.move(PileType.CASCADE, 0, 5, PileType.FOUNDATION, 0);
    noShuffle.move(PileType.CASCADE, 0, 4, PileType.FOUNDATION, 0);
    noShuffle.move(PileType.CASCADE, 0, 3, PileType.FOUNDATION, 0);
    noShuffle.move(PileType.CASCADE, 0, 2, PileType.FOUNDATION, 0);
    noShuffle.move(PileType.CASCADE, 0, 1, PileType.FOUNDATION, 0);
    noShuffle.move(PileType.CASCADE, 0, 0, PileType.FOUNDATION, 0);
    noShuffle.move(PileType.CASCADE, 1, 12, PileType.FOUNDATION, 1);
    noShuffle.move(PileType.CASCADE, 1, 11, PileType.FOUNDATION, 1);
    noShuffle.move(PileType.CASCADE, 1, 10, PileType.FOUNDATION, 1);
    noShuffle.move(PileType.CASCADE, 1, 9, PileType.FOUNDATION, 1);
    noShuffle.move(PileType.CASCADE, 1, 8, PileType.FOUNDATION, 1);
    noShuffle.move(PileType.CASCADE, 1, 7, PileType.FOUNDATION, 1);
    noShuffle.move(PileType.CASCADE, 1, 6, PileType.FOUNDATION, 1);
    noShuffle.move(PileType.CASCADE, 1, 5, PileType.FOUNDATION, 1);
    noShuffle.move(PileType.CASCADE, 1, 4, PileType.FOUNDATION, 1);
    noShuffle.move(PileType.CASCADE, 1, 3, PileType.FOUNDATION, 1);
    noShuffle.move(PileType.CASCADE, 1, 2, PileType.FOUNDATION, 1);
    noShuffle.move(PileType.CASCADE, 1, 1, PileType.FOUNDATION, 1);
    noShuffle.move(PileType.CASCADE, 1, 0, PileType.FOUNDATION, 1);
    noShuffle.move(PileType.CASCADE, 2, 12, PileType.FOUNDATION, 2);
    noShuffle.move(PileType.CASCADE, 2, 11, PileType.FOUNDATION, 2);
    noShuffle.move(PileType.CASCADE, 2, 10, PileType.FOUNDATION, 2);
    noShuffle.move(PileType.CASCADE, 2, 9, PileType.FOUNDATION, 2);
    noShuffle.move(PileType.CASCADE, 2, 8, PileType.FOUNDATION, 2);
    noShuffle.move(PileType.CASCADE, 2, 7, PileType.FOUNDATION, 2);
    noShuffle.move(PileType.CASCADE, 2, 6, PileType.FOUNDATION, 2);
    noShuffle.move(PileType.CASCADE, 2, 5, PileType.FOUNDATION, 2);
    noShuffle.move(PileType.CASCADE, 2, 4, PileType.FOUNDATION, 2);
    noShuffle.move(PileType.CASCADE, 2, 3, PileType.FOUNDATION, 2);
    noShuffle.move(PileType.CASCADE, 2, 2, PileType.FOUNDATION, 2);
    noShuffle.move(PileType.CASCADE, 2, 1, PileType.FOUNDATION, 2);
    noShuffle.move(PileType.CASCADE, 2, 0, PileType.FOUNDATION, 2);
    noShuffle.move(PileType.CASCADE, 3, 12, PileType.FOUNDATION, 3);
    noShuffle.move(PileType.CASCADE, 3, 11, PileType.FOUNDATION, 3);
    noShuffle.move(PileType.CASCADE, 3, 10, PileType.FOUNDATION, 3);
    noShuffle.move(PileType.CASCADE, 3, 9, PileType.FOUNDATION, 3);
    noShuffle.move(PileType.CASCADE, 3, 8, PileType.FOUNDATION, 3);
    noShuffle.move(PileType.CASCADE, 3, 7, PileType.FOUNDATION, 3);
    noShuffle.move(PileType.CASCADE, 3, 6, PileType.FOUNDATION, 3);
    noShuffle.move(PileType.CASCADE, 3, 5, PileType.FOUNDATION, 3);
    noShuffle.move(PileType.CASCADE, 3, 4, PileType.FOUNDATION, 3);
    noShuffle.move(PileType.CASCADE, 3, 3, PileType.FOUNDATION, 3);
    noShuffle.move(PileType.CASCADE, 3, 2, PileType.FOUNDATION, 3);
    noShuffle.move(PileType.CASCADE, 3, 1, PileType.FOUNDATION, 3);
    noShuffle.move(PileType.CASCADE, 3, 0, PileType.FOUNDATION, 3);

    assertEquals(true, noShuffle.isGameOver());


  }

  /**
   * Tests if piles get reset between tests.
   */
  @Test
  public void startGameinBetween() {
    noShuffle = freeCellFactory();
    noShuffle = freeCellFactory();
    noShuffle.startGame(noShuffle.getDeck(), 4, 4,
        false);
    shuffled = freeCellFactory();
    shuffled.startGame(noShuffle.getDeck(), 4, 4,
        false);
    assertEquals(noShuffle.getGameState(), shuffled.getGameState());


  }
  
  















}
