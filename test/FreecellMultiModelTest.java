import static org.junit.Assert.assertEquals;


import cs3500.hw02.PileType;
import cs3500.hw04.FreecellMultiModel;
import org.junit.Before;
import org.junit.Test;

public class FreecellMultiModelTest {

  FreecellMultiModel mult;

  /**
   * Initialize the variables.
   */
  @Before
  public void initalize() {
    mult = new FreecellMultiModel();


  }


  @Test
  /**
   * Tests a multive move.
   */
  public void moveMultipleCascade() {
    mult.startGame(mult.getDeck(), 6, 4, false);
    mult.move(PileType.CASCADE, 0, 8, PileType.FOUNDATION, 0);
    mult.move(PileType.CASCADE, 1, 8, PileType.FOUNDATION, 1);
    mult.move(PileType.CASCADE, 2, 8, PileType.FOUNDATION, 2);

    String gameState = "F1: A♥\n"
        + "F2: A♣\n"
        + "F3: A♦\n"
        + "F4:\n"
        + "O1:\n"
        + "O2:\n"
        + "O3:\n"
        + "O4:\n"
        + "C1: K♥, Q♦, 10♥, 9♦, 7♥, 6♦, 4♥, 3♦\n"
        + "C2: K♣, Q♠, 10♣, 9♠, 7♣, 6♠, 4♣, 3♠\n"
        + "C3: K♦, J♥, 10♦, 8♥, 7♦, 5♥, 4♦, 2♥\n"
        + "C4: K♠, J♣, 10♠, 8♣, 7♠, 5♣, 4♠, 2♣, A♠\n"
        + "C5: Q♥, J♦, 9♥, 8♦, 6♥, 5♦, 3♥, 2♦\n"
        + "C6: Q♣, J♠, 9♣, 8♠, 6♣, 5♠, 3♣, 2♠";

    assertEquals(gameState, mult.getGameState());

    mult.move(PileType.CASCADE, 0 ,7, PileType.OPEN, 0);
    mult.move(PileType.CASCADE, 4, 7, PileType.OPEN, 1);

    gameState = "F1: A♥\n"
        + "F2: A♣\n"
        + "F3: A♦\n"
        + "F4:\n"
        + "O1: 3♦\n"
        + "O2: 2♦\n"
        + "O3:\n"
        + "O4:\n"
        + "C1: K♥, Q♦, 10♥, 9♦, 7♥, 6♦, 4♥\n"
        + "C2: K♣, Q♠, 10♣, 9♠, 7♣, 6♠, 4♣, 3♠\n"
        + "C3: K♦, J♥, 10♦, 8♥, 7♦, 5♥, 4♦, 2♥\n"
        + "C4: K♠, J♣, 10♠, 8♣, 7♠, 5♣, 4♠, 2♣, A♠\n"
        + "C5: Q♥, J♦, 9♥, 8♦, 6♥, 5♦, 3♥\n"
        + "C6: Q♣, J♠, 9♣, 8♠, 6♣, 5♠, 3♣, 2♠";
    assertEquals(gameState, mult.getGameState());
    mult.move(PileType.CASCADE, 5, 7, PileType.CASCADE, 4);
    mult.move(PileType.CASCADE, 2, 7, PileType.CASCADE, 5);
    mult.move(PileType.CASCADE, 5, 6, PileType.CASCADE, 0);
    gameState = "F1: A♥\n"
        + "F2: A♣\n"
        + "F3: A♦\n"
        + "F4:\n"
        + "O1: 3♦\n"
        + "O2: 2♦\n"
        + "O3:\n"
        + "O4:\n"
        + "C1: K♥, Q♦, 10♥, 9♦, 7♥, 6♦, 4♥, 3♣, 2♥\n"
        + "C2: K♣, Q♠, 10♣, 9♠, 7♣, 6♠, 4♣, 3♠\n"
        + "C3: K♦, J♥, 10♦, 8♥, 7♦, 5♥, 4♦\n"
        + "C4: K♠, J♣, 10♠, 8♣, 7♠, 5♣, 4♠, 2♣, A♠\n"
        + "C5: Q♥, J♦, 9♥, 8♦, 6♥, 5♦, 3♥, 2♠\n"
        + "C6: Q♣, J♠, 9♣, 8♠, 6♣, 5♠";
    assertEquals(gameState, mult.getGameState());
    mult.move(PileType.CASCADE, 0, 8, PileType.FOUNDATION, 0);
    gameState = "F1: A♥, 2♥\n"
        + "F2: A♣\n"
        + "F3: A♦\n"
        + "F4:\n"
        + "O1: 3♦\n"
        + "O2: 2♦\n"
        + "O3:\n"
        + "O4:\n"
        + "C1: K♥, Q♦, 10♥, 9♦, 7♥, 6♦, 4♥, 3♣\n"
        + "C2: K♣, Q♠, 10♣, 9♠, 7♣, 6♠, 4♣, 3♠\n"
        + "C3: K♦, J♥, 10♦, 8♥, 7♦, 5♥, 4♦\n"
        + "C4: K♠, J♣, 10♠, 8♣, 7♠, 5♣, 4♠, 2♣, A♠\n"
        + "C5: Q♥, J♦, 9♥, 8♦, 6♥, 5♦, 3♥, 2♠\n"
        + "C6: Q♣, J♠, 9♣, 8♠, 6♣, 5♠";
    assertEquals(gameState, mult.getGameState());
  }

  /**
   * Tests a multi move to Foundation.
   */
  @Test
  public void moveMultipleFoundation() {

    mult.startGame(mult.getDeck(), 6, 4, false);
    mult.move(PileType.CASCADE, 2, 8, PileType.FOUNDATION, 0);
    mult.move(PileType.CASCADE, 4, 7, PileType.FOUNDATION, 0);
    String gameState = "F1: A♦, 2♦\n"
        + "F2:\n"
        + "F3:\n"
        + "F4:\n"
        + "O1:\n"
        + "O2:\n"
        + "O3:\n"
        + "O4:\n"
        + "C1: K♥, Q♦, 10♥, 9♦, 7♥, 6♦, 4♥, 3♦, A♥\n"
        + "C2: K♣, Q♠, 10♣, 9♠, 7♣, 6♠, 4♣, 3♠, A♣\n"
        + "C3: K♦, J♥, 10♦, 8♥, 7♦, 5♥, 4♦, 2♥\n"
        + "C4: K♠, J♣, 10♠, 8♣, 7♠, 5♣, 4♠, 2♣, A♠\n"
        + "C5: Q♥, J♦, 9♥, 8♦, 6♥, 5♦, 3♥\n"
        + "C6: Q♣, J♠, 9♣, 8♠, 6♣, 5♠, 3♣, 2♠";
    assertEquals(gameState, mult.getGameState());


  }


  /**
   * Tests a multi move when there arent enough open slots.
   */
  @Test(expected = IllegalArgumentException.class)
  public void notEnoughOpenSlots() {
    mult.startGame(mult.getDeck(), 6, 4, false);

    mult.move(PileType.CASCADE, 2, 8, PileType.OPEN, 0);
    mult.move(PileType.CASCADE, 4, 7, PileType.OPEN, 1);
    mult.move(PileType.CASCADE, 5, 7, PileType.OPEN, 2);
    mult.move(PileType.CASCADE, 3, 8, PileType.OPEN, 3);



    mult.move(PileType.CASCADE, 0, 8, PileType.CASCADE, 3);
    System.out.print(mult.getGameState());

    mult.move(PileType.CASCADE, 3, 7, PileType.CASCADE, 5);

  }

  /**
   * Tests an invalid multi move to an open pile.
   */
  @Test(expected = IllegalArgumentException.class)
  public void multiMoveOpen() {
    mult.startGame(mult.getDeck(), 6, 4, false);
    mult.move(PileType.CASCADE, 2, 7, PileType.OPEN, 0);
  }




}