package cs3500.hw04;

import cs3500.hw02.Card;
import cs3500.hw02.FreecellOperations;
import cs3500.hw02.PileType;
import cs3500.hw02.Suits;
import cs3500.hw02.Values;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Stack;

/**
 * Class enables the free cell model to move multiple cards at once.
 * This model uses abstract piles with cascade, open, and foundation types
 * compared to the single move model which uses lists of stacks.
 */
public class FreecellMultiModel implements FreecellOperations<Card> {

  /**
   * Default constructor.
   */

  private List<Card> deck;
  private CascadePile[] cascadePiles;
  private FoundationPile[] foundationPiles;
  private OpenPile[] openPiles;
  private boolean gameStart;
  private HashSet<Card> duplicates;
  private HashMap<PileType, Pile[]> piles;


  /**
   * Constructor that initalizes all of the instance variables.
   */
  public FreecellMultiModel() {
    this.deck = this.getDeck();
    this.cascadePiles = new CascadePile[0];
    this.foundationPiles = new FoundationPile[4];
    this.openPiles = new OpenPile[0];
    this.duplicates = new HashSet<Card>();
    this.gameStart = false;
    this.piles = new HashMap<>();
    this.piles.put(PileType.CASCADE, cascadePiles);
    this.piles.put(PileType.FOUNDATION, foundationPiles);
    this.piles.put(PileType.OPEN, openPiles);

  }



  /**
   * Checks the Deck for duplicaes.
   * @param deck to be checked for no duplicates.
   * @return whether it is a valid deck with no duplicates
   */
  private boolean checkDuplicates(List<Card> deck) {

    if (deck.size() != 52) {
      return false;
    }

    this.duplicates.clear();

    for (int i = 0; i < deck.size(); i ++) {
      this.duplicates.add(deck.get(i));
    }

    return this.duplicates.size() == 52;
  }



  /**
   * Initalizes the deck with all possible valid cards.
   * @return a deck with all cards possible
   */

  public List<Card> getDeck() {


    List<Card> deck = new LinkedList<Card>();
    for (Values value: Values.values()) {
      for (Suits suit: Suits.values()) {
        deck.add(new Card(value, suit));
      }
    }



    return deck;
  }


  /**
   * Copies each Card in the Deck.
   * @param deck to be copied.
   * @return a shallow copy of the deck
   */
  private List<Card> copyDeck(List<Card> deck) {
    List<Card> copyDeck = new Stack<Card>();

    for (int i = 0; i < deck.size(); i++) {
      copyDeck.add(deck.get(i).clone());
    }

    return copyDeck;

  }

  /**
   * Shuffles the given deck.
   *
   * @param deck to be shuffled.
   * @return a shuffled deck
   */

  private List<Card> shuffle(List<Card> deck) {
    List<Card> shuffledDeck = new Stack<Card>();
    Random random = new Random();
    for (int i = deck.size(); i > 0; i--) {
      shuffledDeck.add(deck.remove(random.nextInt(i)));
    }


    return shuffledDeck;

  }


  /**
   * Deal a new game of freecell with the given deck, with or without shuffling
   * it first. This method first verifies that the deck is valid. It deals the
   * deck among the cascade piles in roundrobin fashion. Thus if there are 4
   * cascade piles, the 1st pile will get cards 0, 4, 8, ..., the 2nd pile will
   * get cards 1, 5, 9, ..., the 3rd pile will get cards 2, 6, 10, ... and the
   * 4th pile will get cards 3, 7, 11, .... Depending on the number of cascade
   * piles, they may have a different number of cards
   * @param deck            the deck to be dealt
   * @param numCascadePiles number of cascade piles
   * @param numOpenPiles    number of open piles
   * @param shuffle         if true, shuffle the deck else deal the deck as-is
   */
  @Override
  public void startGame(List<Card> deck , int numCascadePiles, int numOpenPiles, boolean shuffle)
      throws IllegalArgumentException {

    if (deck.size() != 52) {
      throw new IllegalArgumentException("Not enough cards in Deck");
    }


    if (numCascadePiles < 4 || numOpenPiles < 1) {
      throw new IllegalArgumentException("Not enough piles");
    }

    List<Card> copiedDeck = this.copyDeck(deck);
    if (shuffle) {
      List<Card> shuffledDeck = this.copyDeck(deck);
      this.deck = this.shuffle(shuffledDeck);
      copiedDeck = this.shuffle(copiedDeck);

    }

    if (!checkDuplicates(copiedDeck)) {
      throw new IllegalArgumentException("Invalid Deck");
    }



    this.gameStart = true;


    this.cascadePiles = new CascadePile[numCascadePiles];
    this.openPiles = new OpenPile[numOpenPiles];



    /*
      initialize the correct number of piles for the Cascade, Open, and Foundation Piles
     */
    for (int i = 0; i < numCascadePiles; i++) {
      this.cascadePiles[i] = new CascadePile();
    }
    for (int i = 0; i < numOpenPiles; i++) {
      this.openPiles[i] = new OpenPile();

    }

    for (int i = 0; i < 4; i++) {
      this.foundationPiles[i] = new FoundationPile();
    }


    /*
      Distribute the deck amongst the cascade piles round robin.
     */

    for (int i = 0; i < this.deck.size(); i++) {
      this.cascadePiles[i % numCascadePiles].addCard(copiedDeck.remove(0));
    }

    this.piles.put(PileType.OPEN, openPiles);
    this.piles.put(PileType.CASCADE, cascadePiles);
    this.piles.put(PileType.FOUNDATION, foundationPiles);



  }

  /**
   * Return the present state of the game as a string. The string is formatted
   * as follows:
   * <pre>
   * F1:[b]f11,[b]f12,[b],...,[b]f1n1[n] (Card in foundation pile 1 in order)
   * F2:[b]f21,[b]f22,[b],...,[b]f2n2[n] (Card in foundation pile 2 in order)
   * ...
   * Fm:[b]fm1,[b]fm2,[b],...,[b]fmnm[n] (Card in foundation pile m in
   * order)
   * O1:[b]o11[n] (Card in open pile 1)
   * O2:[b]o21[n] (Card in open pile 2)
   * ...
   * Ok:[b]ok1[n] (Card in open pile k)
   * C1:[b]c11,[b]c12,[b]...,[b]c1p1[n] (Card in cascade pile 1 in order)
   * C2:[b]c21,[b]c22,[b]...,[b]c2p2[n] (Card in cascade pile 2 in order)
   * ...
   * Cs:[b]cs1,[b]cs2,[b]...,[b]csps (Card in cascade pile s in order)
   *
   * where [b] is a single blankspace, [n] is newline. Note that there is no
   * newline on the last line
   * </pre>
   *
   * @return the formatted string as above
   */
  @Override
  public String getGameState() {
    if (!gameStart) {
      return "";
    }

    String gameState = "";

    for (int i = 0; i < foundationPiles.length ; i++) {
      gameState += String.format("F%d:", i + 1) + foundationPiles[i].getGameStatePile() + "\n";
    }


    for (int i = 0; i < openPiles.length; i++) {
      //gameState += String.format("O%d:", i + 1) + openPiles[i].toString() + "\n";
      gameState += String.format("O%d:", i + 1) + openPiles[i].getGameStatePile() + "\n";

    }

    for (int i = 0; i < cascadePiles.length; i++) {
      //gameState += String.format("C%d:", i + 1) + cascadePiles[i].toString() + "\n";
      gameState += String.format("C%d:", i + 1) + cascadePiles[i].getGameStatePile() + "\n";

    }

    String s =  gameState.substring(0, gameState.length() - 1 );

    return gameState.substring(0, gameState.length() - 1 );
  }

  /**
   * Signal if the game is over or not.
   * @return true if game is over.False if not
   */
  @Override
  public boolean isGameOver() {
    int totalNumCards = 0;
    for (int i = 0 ; i < this.foundationPiles.length; i++) {
      totalNumCards += this.foundationPiles[i].size();
    }

    return totalNumCards == 52;
  }

  /**
   * Move a card from the given source pile to the given destination pile, if the move is valid.
   *
   * @param sourceType the type of the source pile see @link{PileType}
   * @param sourcePileNumber the pile number of the given type, starting at 0
   * @param cardIndex the index of the card to be moved from the source pile, starting at 0
   * @param destinationType the type of the destination pile (see
   * @param destPileNumber the pile number of the given type, starting at 0
   * @throws IllegalArgumentException if the move is not possible {@link PileType})
   */
  @Override
  public void move(PileType sourceType, int sourcePileNumber, int cardIndex,
      PileType destinationType, int destPileNumber) {

    if (sourcePileNumber < 0 | destPileNumber < 0 | cardIndex < 0) {
      throw new IllegalArgumentException("Entered a negative index");
    }
    int numCardMove = (int)this.maxNumCards();



    List<Card> sourcePile = this.piles.get(sourceType)[sourcePileNumber].getMovePile(cardIndex);

    if (sourcePile.size() > numCardMove) {
      throw new IllegalArgumentException("Cannot move this many cards");
    }

    //String destinationPile = destinationType.name();

    this.piles.get(destinationType)[destPileNumber].append(sourcePile);
    this.piles.get(sourceType)[sourcePileNumber].removeCardsStartingFrom(cardIndex);


  }

  /**
   * Calculates number of cards that can be moved at once.
   * @return number of cards that can be moved at once.
   */

  private double maxNumCards() {

    int numCascade  = 0 ;
    int numOpen = 0;
    for (int i = 0; i < this.cascadePiles.length ; ++i) {
      if (this.cascadePiles[i].getPile().size() == 0) {
        numCascade++;
      }
    }

    for (int i = 0; i < this.openPiles.length; ++i) {
      if ( this.openPiles[i].getPile().size() == 0) {
        numOpen++;
      }
    }


    return (numOpen + 1) * (Math.pow(2, numCascade));
  }


}
