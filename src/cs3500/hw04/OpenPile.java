package cs3500.hw04;

import cs3500.hw02.Card;
import java.util.List;
import java.util.Stack;

public class OpenPile extends Pile {

  /**
   * Default constructor.
   * @param pile an OpenPile.
   */
  public OpenPile(Stack<Card> pile) {
    super(pile);
  }

  public OpenPile() {
    super();
  }


  /**
   * Get the move pile.
   * @param cardIndex return all cards starting from the param.
   * @return pile of cards that will be moved
   */
  @Override
  public List<Card> getMovePile(int cardIndex) {
    if (this.getPile().size() == 0) {
      throw new IllegalArgumentException("Cannot remove from this open pile");
    }

    else {
      return (Stack<Card>)this.getPile().clone();
    }
  }



  /**
   * Add this list of cards to the end of this pile.
   * @param toAppend list of cards that will be added to this pile
   */
  @Override
  public void append(List<Card> toAppend) {
    if (this.getPile().size() == 0 && this.validAdd(toAppend)) {
      this.getPile().add(toAppend.get(0));
    }

    else {
      throw new IllegalArgumentException("This Open Pile is already full");
    }
  }

  /**
   * Does this list of cards form a valid build.
   * @param movePile list of cards taht are checked for the build
   * @return whether the list of cards are a valid build of cards
   */

  @Override
  public boolean validAdd(List<Card> movePile) {
    return movePile.size() == 1;
  }



}
