package cs3500.hw04;

import cs3500.hw02.Card;
import java.util.List;
import java.util.Stack;


/**
 * Abstract class of the Pile. The pile can either be an open, foundation , or cascade pile.
 */
public abstract class Pile {

  private Stack<Card> pile;

  /**
   * Constructor that assigns this pile to the passed in pile.
   * @param pile the stack of cards this pile should use
   */
  Pile(Stack<Card> pile) {
    this.pile = pile;
  }


  /**
   * Default Constructor.
   */
  Pile() {
    this.pile = new Stack<Card>();
  }

  void addCard(Card c) {
    this.pile.add(c);
  }

  protected Stack<Card> getPile() {
    return this.pile;
  }


  /**
   * Prints the location of all the cards in the piles.
   * @return the state of the game
   */
  public String getGameStatePile() {
    String allCards = "";
    for (Card c: pile) {
      allCards += " " + c.toString() + ",";
    }

    if (allCards.length() != 0) {
      allCards = allCards.substring(0, allCards.length() - 1);
    }

    return allCards;
  }

  /**
   * Returns the size of the pile.
   * @return size of the pile
   */

  public int size() {
    return this.pile.size();
  }


  /**
   * The pile of the cards that will be moved.
   * @param cardIndex get the cards in the pile starting at this param
   * @return pile of cards that form a valid build that can be moved
   */
  public abstract List<Card> getMovePile(int cardIndex);


  /**
   * Add this list of cards to the end of this pile.
   * @param appended list of cards that will be added to this pile
   */
  public abstract void append(List<Card> appended);


  /**
   * Get a copy of this pile.
   * @return a copy of the pile
   */
  List<Card> getCopy() {
    return (Stack<Card>) this.pile.clone();
  }


  /**
   * Does this list of cards form a valid build.
   * @param movePile list of cards that are checked for the build
   * @return whether the list of cards are a valid build of cards
   */
  public abstract boolean validAdd(List<Card> movePile);


  /**
   * Removes cards from the pile starting at the cardIndex.
   * @param cardIndex starting from this param remove the cards at the end of this pile
   */
  public void removeCardsStartingFrom(int cardIndex) {
    int num_deletes = this.getPile().size() - cardIndex;
    for (int i = 0; i < num_deletes; ++i) {
      this.getPile().pop();
    }
  }

}
