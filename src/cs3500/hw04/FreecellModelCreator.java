package cs3500.hw04;

import cs3500.hw02.FreecellModel;
import cs3500.hw02.FreecellOperations;

/**
 * Factory Class for creating single and multi move models.
 */

public class FreecellModelCreator {

  public enum GameType {
    SINGLEMOVE,
    MULTIMOVE;
  }

  /**
   * Creates either a single or multi move freecell model.
   * @param type type of model to create
   * @return returns the indtended freecell model
   */

  public static FreecellOperations create(GameType type) {

    if (type == GameType.SINGLEMOVE) {
      return new FreecellModel();
    }

    else {
      return new FreecellMultiModel();
    }

  }


}
