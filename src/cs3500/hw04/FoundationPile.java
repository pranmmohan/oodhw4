package cs3500.hw04;

import cs3500.hw02.Card;
import java.util.List;


/**
 * A foundation pile. Can only contains cards of the same suit and one value less than the
 * card that comes before it.
 */

public class FoundationPile extends Pile {

  /**
   * Default Constructor.
   */

  public FoundationPile() {
    super();
  }


  /**
   * Get the move pile.
   * @param cardIndex return all cards starting from the param.
   * @return pile of cards that will be moved
   */
  @Override
  public List<Card> getMovePile(int cardIndex) {
    if (cardIndex >= this.getPile().size()) {
      throw new IllegalArgumentException("Card Index too large for this Foundation Pile");
    }

    else {
      return this.getCopy().subList(cardIndex, this.getPile().size());
    }
  }


  /**
   * Add this list of cards to the end of this pile.
   * @param appended list of cards that will be added to this pile
   */
  @Override
  public void append(List<Card> appended) {

    boolean validAppend = validAdd(appended);
    if (this.getPile().size() == 0  & validAppend) {
      this.getPile().addAll(appended);
    }

    else if (validAppend
        && this.getPile().get(this.getPile().size() - 1).validFoundationMove(appended.get(0))) {
      this.getPile().addAll(appended);
    }
    else {
      throw new IllegalArgumentException("These Cards cannot be moved onto a Foundation Pile");
    }

  }

  /**
   * Does this list of cards form a valid build.
   * @param movePile list of cards taht are checked for the build
   * @return whether the list of cards are a valid build of cards
   */
  @Override
  public boolean validAdd(List<Card> movePile) {
    Card compareCard = movePile.get(0);

    for (int i = 1; i < movePile.size() - 1; i ++) {
      if (!compareCard.validFoundationMove(movePile.get(i))) {
        return false;
      }
      compareCard = movePile.get(i);
    }

    return true;
  }
}
