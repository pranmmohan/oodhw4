package cs3500.hw02;

public enum Suits {
  Heart(Character.toString('\u2665')),
  Club(Character.toString('\u2663')),
  Diamond(Character.toString('\u2666')),
  Spade(Character.toString('\u2660'));

  private final String symbol;


  /**
   * Constructor.
   * @param symbol the suit of the card.
   */
  Suits(String symbol) {
    this.symbol = symbol;
  }



  @Override
  public  String toString() {
    return this.symbol;
  }



  /**
   * Checks the suit to see if it is the opposite color.
   *
   * @param card  suit of the card to be checked
   * @return whether the card's is the opposite color
   */
  protected  boolean oppositeColors(Suits card) {


    if (card == Suits.Diamond || card == Suits.Heart) {
      return (this == Suits.Club || this == Suits.Spade);
    }

    else if (card == Suits.Club || card == Suits.Spade) {
      return (this == Suits.Heart || this == Suits.Diamond);
    }

    else {
      throw new IllegalArgumentException("Invalid Card Suit");
    }

  }


}
