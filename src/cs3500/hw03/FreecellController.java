package cs3500.hw03;

import cs3500.hw02.Card;
import cs3500.hw02.FreecellOperations;
import cs3500.hw02.PileType;
import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.HashMap;


/**
 * This is the class of the controller. It takes in inputs, starts the game and makes
 * the moves in the provided model.
 */

public class FreecellController implements IFreecellController<Card> {



  private Readable rd;
  private Appendable ap;
  HashMap<String, PileType> charToPileType = new HashMap<>();


  /**
   * Constructor that checks whether either param is null.
   * @param rd  the input set of commands.
   * @param ap  the output to which all messages are written to.
   */
  public FreecellController(Readable rd, Appendable ap) {

    if (ap == null | rd == null) {
      throw new IllegalStateException("Either empty readable or appendable");
    }

    this.rd = rd;
    this.ap = ap;


    this.charToPileType.put("C", PileType.CASCADE);
    this.charToPileType.put("O", PileType.OPEN);
    this.charToPileType.put("F", PileType.FOUNDATION);

  }

  /**
   * Must be an integer and > 0.
   * @param integer string to check if it is an integer.
   * @return  whether the string is an actual integer
   */
  private boolean validateInteger(String integer) {

    try {
      int index = Integer.parseInt(integer);
      return index > 0;
    }
    catch (NumberFormatException e) {
      return false;
    }

  }

  /**
   * Checks if the source is a valid pile type character.
   * @param source  string to check for valid pile character
   * @param scan    contains the next string in the input
   * @return  Either a q if the end of the input has been hit or next validPile in input.
   */
  private String validatePile(String source, Scanner scan) {

    if (validateInteger(source.substring(1))) {

      switch (source.substring(0, 1)) {
        case "C":
          return source;
        case "O":
          return source;
        case "F":
          return source;
        case "Q":
          return source;
        case "q":
          return source;
        default:

          if (scan.hasNext()) {
            return validatePile(scan.next(), scan);
          }
          break;
      }
    }

    else if (scan.hasNext()) {
      validatePile(scan.next(), scan);
    }

    return "q";
  }


  /**
   * Checks if the pile number attached to a pile type is a valid number.
   * @param source string that attempts to become an integer
   * @param scan   input
   * @return Either a Q if the end of the input has been hit or next validPilePosition in input.
   */
  String validatePosition(String source, Scanner scan) {
    if (source.equalsIgnoreCase("q") || validateInteger(source)) {
      return source;
    }

    else if (scan.hasNext()) {
      return validatePosition(scan.next(), scan);
    }

    return "q";


  }

  /**
   * Appends a string to the output.
   */
  void append(String output) {
    try {
      this.ap.append(output);
    }
    catch (IOException e) {
      return;
    }
  }


  /**
   * Initializes the model and starts a game with the inputted parameters. Continuously reads from
   * the readable and tries to make moves until there is no more input or the game is over.
   * @param deck          the deck used to deal the starting piles
   * @param model         the model which the game is played with
   * @param numCascades   number of cascade piles to be initialized
   * @param numOpens      number of open piles to be initialized
   * @param shuffle       should the deck be shuffled first before its dealt
   */
  @Override
  public void playGame(List<Card> deck, FreecellOperations<Card> model,  int numCascades,
      int numOpens, boolean shuffle) {


    if (deck == null || model == null) {
      throw new IllegalArgumentException("You have entered either an empty deck or model");
    }



    try {
      model.startGame(deck, numCascades, numOpens, shuffle);
      this.append(model.getGameState());

    }
    catch (IllegalArgumentException e) {
      this.append("Could not start game.");
      return;
    }



    Scanner scan = new Scanner(rd).useDelimiter("\\s+");

    if (!scan.hasNext()) {
      throw new IllegalStateException("No input provided");
    }

    while (scan.hasNext()) {


      try {
        String source = scan.next();
        source = validatePile(source, scan);
        if (source.equalsIgnoreCase("q")) {
          this.append("\nGame quit prematurely.");
          return;
        }


        String position = scan.next();
        position = validatePosition(position, scan);
        if (position.equalsIgnoreCase("q")) {
          this.append("\nGame quit prematurely.");
          return;

        }


        String destination = scan.next();
        destination = validatePile(destination, scan);
        if (destination.equalsIgnoreCase("q")) {
          this.append("\nGame quit prematurely.");
          return;

        }



        else {
          String sourcePileType = source.substring(0, 1);
          Integer sourcePileNumber = Integer.parseInt(source.substring(1)) - 1;

          int cardIndex = Integer.parseInt(position) - 1;

          String destinationPileType = destination.substring(0, 1);
          Integer destinationPileNumber = Integer.parseInt(destination.substring(1)) - 1;

          try {
            model.move(charToPileType.get(sourcePileType), sourcePileNumber,
                cardIndex, charToPileType.get(destinationPileType), destinationPileNumber);
            this.append("\n" + model.getGameState());
          }

          catch (IllegalArgumentException e) {
            this.append("\nInvalid move. Try again.");

          }

          // one or more of the arguemnts was null
          catch (NullPointerException e) {
            return;
          }


        }


      }
      // not enough arguments passed in
      catch (NoSuchElementException e) {
        this.append("\nGame quit prematurely.");
        return;
      }


      if (model.isGameOver()) {
        this.append("\nGame over.");
        return;
      }

    }

  }

}
