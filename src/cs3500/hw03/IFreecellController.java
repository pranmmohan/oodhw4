package cs3500.hw03;

import cs3500.hw02.FreecellOperations;
import java.util.List;


/**
 * This is the interface freecell controller. It is paramaetrized over K so that it can implement
 * any implementation of a card object.
 */
public interface IFreecellController<K> {


  /**
   * Using the passed in parameters starts the game in the model with the passed parameters.
   * @param deck          the deck used to deal the starting piles
   * @param model         the model which the game is played with
   * @param numCascades   number of cascade piles to be initialized
   * @param numOpens      number of open piles to be initialized
   * @param shuffle       should the deck be shuffled first before its dealt
   */
  void playGame(List<K> deck, FreecellOperations<K> model,
      int numCascades, int numOpens, boolean shuffle);
}
